import React, { Component } from 'react';
import TableHeaders from './TableHeaders/TableHeaders';
import TableBody from './TableBody/TableBody';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner';
import { connect } from 'react-redux';
import * as axios from '../../axios';
import * as actions from '../../store/actions/actions';
import Backdrop from '../../components/UI/Backdrop/Backdrop';

// component used for displaying the table
class Table extends Component {
	state = {
		tableHeaders: [
			'Book name',
			'Edition',
			'Writer',
			'Nubmer of votes',
			'Publisher',
			'Options'
		]
	};

	// gets data from db
	componentDidMount() {
		this.props.getData();
	}

	// deletes a book with the diven ID
	deleteData = (id) => {
		axios.bookInstance.delete(`${id}`).then(() => this.props.getData());
	};

	render() {
		// renders table body
		let tableBody = <TableBody data={this.props.tableData} delete={this.deleteData} />;

		// renders a spinner while data is being fetched
		if (this.props.dataIsFetching) {
			tableBody = (
				<tbody>
					<tr>
						<td colSpan={this.state.tableHeaders.length}>
							<Spinner />
						</td>
					</tr>
				</tbody>
			);
		}

		return (
			<div className="row">
				{/* renders a error message in case of failing to retrieve data */}
				{this.props.error ? <Backdrop text="Failed to load data" click={this.props.errorHandler} /> : null}

				<div className="col-lg-10">
					<div className="btn-group float-right">
						<Button
							type="primary"
							text="Previous"
							disabled={this.props.page === 0}
							click={() => this.props.changePage(-1)}
						/>
						<Button
							type="primary"
							text="Next"
							disabled={this.props.page === this.props.totalPages - 1}
							click={() => this.props.changePage(1)}
						/>
					</div>
					<table className="table">
						<TableHeaders headers={this.state.tableHeaders} />
						{tableBody}
					</table>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		searchQueries: state.searchReducer.search,
		tableData: state.tableReducer.tableData,
		page: state.tableReducer.page,
		totalPages: state.tableReducer.totalPages,
		dataIsFetching: state.tableReducer.dataIsFetching,
		error: state.errorReducer.errorFetchingData
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		// gets data from db
		getData: () => dispatch(actions.onGetTableData()),
		// updates the page value
		changePage: (value) => dispatch(actions.onChangePage(value)),
		// used for displaying an error
		errorHandler: () => dispatch(actions.errorFetchingData(false))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Table);
