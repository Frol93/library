import React from 'react';
import Button from '../../../components/UI/Button/Button';
import { withRouter } from 'react-router-dom';

// component used for displaying table data using passed props
const tableBody = (props) => {
	const tableData = props.data.map((book) => {
		return (
			<tr key={book.isbn}>
				<td>{book.name}</td>
				<td>{book.edition}</td>
				<td>{book.writer}</td>
				<td>{book.votes}</td>
				<td>{book.publisherName}</td>
				<td colSpan="2">
					<Button type="danger" text="Obrisi" click={() => props.delete(book.id)} />
					<Button type="warning" text="Izmeni" click={() => props.history.push(`/edit/${book.id}`)} />
				</td>
			</tr>
		);
	});

	return <tbody>{tableData}</tbody>;
};

export default withRouter(tableBody);
