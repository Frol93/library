import React from 'react';

// component used for displaying table headers
const tableHeaders = (props) => {
	const headers = props.headers.map((header) => {
		return <th key={header}>{header}</th>;
	});

	return (
		<thead className="thead-dark">
			<tr>{headers}</tr>
		</thead>
	);
};

export default tableHeaders;
