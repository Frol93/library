import React, { Component } from 'react';
import Input from '../../components/Input/Input';
import Button from '../../components/UI/Button/Button';
import Backdrop from '../../components/UI/Backdrop/Backdrop';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/actions';

// component used to send new book data to db
class NewBook extends Component {
	state = {
		disabled: true
	};

	// gets publishers from db
	componentDidMount() {
		this.props.getPublishers();
	}

	// disables the add button if there is an empty field
	willDisable = () => {
		return Object.keys(this.props.newBook).map((key) => this.props.newBook[key].value.trim()).includes('');
	};

	// renders input fields from props
	render() {
		const inputs = Object.keys(this.props.newBook).map((key) => {
			return (
				<Input
					key={key}
					elementName={key}
					value={this.props.newBook[key].value}
					change={this.props.onChange}
					element={this.props.newBook[key].element}
					type={this.props.newBook[key].type}
					options={this.props.publishers}
				/>
			);
		});

		let button = <Button type="primary" click={this.props.onSubmit} text="Add" disabled={this.willDisable()} />;
		// displays a spinner while the book is being sent
		if (this.props.bookIsSending) {
			button = (
				<Button
					type="outline-primary"
					text={
						<span
							className="spinner-border text-primary spinner-border-sm"
							role="status"
							aria-hidden="true"
						/>
					}
					disabled={true}
				/>
			);
		}

		return (
			<div className="col-lg-6">
				{/* displays an error message if error prop is true */}
				{this.props.error ? (
					<Backdrop text="Failed to send data, duplicate ISBN entry" click={this.props.errorHandler} />
				) : null}
				{inputs}
				{button}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		newBook: state.bookReducer.book,
		publishers: state.bookReducer.publishers,
		bookIsSending: state.bookReducer.bookIsSending,
		error: state.errorReducer.errorPostingData
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		// handles input change event
		onChange: (event, elementName) => dispatch(actions.onInputChange(event, elementName)),
		// handles submit
		onSubmit: () => dispatch(actions.onSubmit()),
		// gets publishers from db
		getPublishers: () => dispatch(actions.getPublishers()),
		// displays an error
		errorHandler: () => dispatch(actions.errorPostingData(false))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(NewBook);
