import React, { Component } from 'react';
import Input from '../../components/Input/Input';
import Button from '../../components/UI/Button/Button';
import { connect } from 'react-redux';
import * as axios from '../../axios';
import * as actions from '../../store/actions/BookActions';

// component used for editing details of a specific book
class EditBook extends Component {
	state = {
		bookFromState: {},
		bookToEdit: {}
	};

	// gets a specific book from db using an ID param
	componentDidMount() {
		this.props.getPublishers();

		axios.bookInstance.get(`${this.props.match.params.id}`).then((res) => {
			const book = this.props.editBook;
			for (let key in book) {
				book[key].value = res.data[key];
			}
			this.setState({ bookToEdit: res.data, bookFromState: book });
		});
	}

	// handles input change
	onChange = (event, elementName) => {
		const bookFromState = { ...this.state.bookFromState };
		bookFromState[elementName].value = event.target.value;
		this.setState({ bookFromState });
	};

	// sends the book do db with updated data
	onEdit = () => {
		const editedBook = { ...this.state.bookToEdit };
		for (let key in this.state.bookFromState) {
			editedBook[key] = this.state.bookFromState[key].value;
		}

		axios.bookInstance.put(`${this.props.match.params.id}`, editedBook).then(() => {
			this.props.history.push('/');
		});
	};

	render() {
		// renders input fields from props
		const inputs = Object.keys(this.state.bookFromState).map((key) => {
			return (
				<Input
					key={key}
					elementName={key}
					value={this.state.bookFromState[key].value}
					change={this.onChange}
					element={this.state.bookFromState[key].element}
					type={this.state.bookFromState[key].type}
					options={this.props.publishers}
				/>
			);
		});

		return (
			<div className="row">
				<div className="col-lg-6">
					{inputs} <Button type="primary" click={this.onEdit} text="Edit" />
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		// creates a copy of a book object from the bookReducer
		editBook: JSON.parse(JSON.stringify(state.bookReducer.book)),
		publishers: state.bookReducer.publishers
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		// gets the publishers from db
		getPublishers: () => dispatch(actions.getPublishers())
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(EditBook);
