import axios from 'axios';

export const bookInstance = axios.create({
	baseURL: '/api/books'
});

export const publisherInstance = axios.create({
	baseURL: '/api/publishers'
});
