import * as ActionTypes from './ActionTypes';

// handles input change event
export const onSearchInputChange = (event, elementName) => {
	return {
		type: ActionTypes.ON_SEARCH_INPUT_CHANGE,
		event: event.target.value,
		elementName
	};
};
