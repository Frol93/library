import * as ActionTypes from './ActionTypes';

// used for displaying/removing error message in case of retrieving table data from db
export const errorFetchingData = (error) => {
	return {
		type: ActionTypes.ERROR_FETCH_DATA,
		error
	};
};

// used for displaying/removing error message in case of failing to post new book details
export const errorPostingData = (error) => {
	return {
		type: ActionTypes.ERROR_POST_DATA,
		error
	};
};
