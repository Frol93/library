import * as axios from '../../axios';
import * as ActionTypes from './ActionTypes';
import store from '../store';
import { onGetTableData } from './TableActions';
import { errorPostingData } from './ErrorActions';

// handles input change for editing a book, and creating a new one
export const onInputChange = (event, elementName) => {
	return {
		type: ActionTypes.ON_INPUT_CHANGE,
		event: event.target.value,
		elementName
	};
};

// clears input fields
export const resetBookValues = () => {
	return {
		type: ActionTypes.RESET_BOOK_VALUES
	};
};

// used for displaying/removing spinner
const bookIsSending = (value) => {
	return {
		type: ActionTypes.BOOK_SPINNER,
		value
	};
};

// handles submit logic
export const onSubmit = () => {
	return (dispatch) => {
		dispatch(bookIsSending(true));
		const { book } = store.getState().bookReducer;
		const newBook = {};
		for (let b in book) {
			newBook[b] = book[b].value;
		}
		axios.bookInstance
			.post('', newBook)
			.then(() => {
				dispatch(bookIsSending(false));
				dispatch(resetBookValues());
				dispatch(onGetTableData());
			})
			.catch(() => {
				dispatch(bookIsSending(false));
				dispatch(errorPostingData(true));
			});
	};
};

// gets publishers from db
export const getPublishers = () => {
	return (dispatch) => {
		axios.publisherInstance.get().then((res) => dispatch(setPublishers(res.data)));
	};
};

// sets the state
const setPublishers = (publishers) => {
	return {
		type: ActionTypes.SET_PUBLISHERS,
		publishers
	};
};
