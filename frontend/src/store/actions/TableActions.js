import * as axios from '../../axios';
import * as ActionTypes from './ActionTypes';
import store from '../store';
import { errorFetchingData } from './ErrorActions';

// used for displaying/removing a spinner in case of fetching table data from db
const dataIsFetching = (value) => {
	return {
		type: ActionTypes.TABLE_SPINNER,
		value
	};
};

// used to set table data
const setTableData = (data, headers) => {
	return {
		type: ActionTypes.SET_TABLE_DATA,
		data,
		totalPages: headers.totalpages
	};
};

// used for retrieving table data from db
export const onGetTableData = () => {
	return (dispatch) => {
		dispatch(dataIsFetching(true));
		const { search } = store.getState().searchReducer;
		const { page } = store.getState().tableReducer;
		const config = { params: {} };

		// sets search params
		if (search.name.value !== '') {
			config.params.name = search.name.value;
		}
		if (search.writer.value !== '') {
			config.params.writer = search.writer.value;
		}
		if (search.votes.value !== '') {
			config.params.votes = search.votes.value;
		}

		// sets page number
		config.params.page = page;

		axios.bookInstance
			.get('', config)
			.then((res) => {
				dispatch(dataIsFetching(false));
				dispatch(setTableData(res.data, res.headers));
			})
			.catch(() => {
				dispatch(dataIsFetching(false));
				dispatch(errorFetchingData(true));
			});
	};
};

// resets page back to 0
export const resetPage = () => {
	return {
		type: ActionTypes.RESET_PAGE
	};
};

// updates page value
const changePage = (value) => {
	return {
		type: ActionTypes.ON_CHANGE_PAGE,
		value
	};
};

// retrives data and updates page value
export const onChangePage = (value) => {
	return (dispatch) => {
		dispatch(changePage(value));
		dispatch(onGetTableData());
	};
};
