export const ON_INPUT_CHANGE = 'ON_INPUT_CHANGE';
export const RESET_BOOK_VALUES = 'RESET_BOOK_VALUES';
export const SET_PUBLISHERS = 'SET_PUBLISHERS';
export const SET_EDIT_DETAILS = 'SET_EDIT_DETAILS';
export const ON_SEARCH_INPUT_CHANGE = 'ON_SEARCH_INPUT_CHANGE';
export const SET_TABLE_DATA = 'SET_TABLE_DATA';
export const ON_CHANGE_PAGE = 'ON_CHANGE_PAGE';
export const RESET_PAGE = 'RESET_PAGE';
export const BOOK_SPINNER = 'BOOK_SPINNER';
export const TABLE_SPINNER = 'TABLE_SPINNER';
export const ERROR_FETCH_DATA = 'ERROR_FETCH_DATA';
export const ERROR_POST_DATA = 'ERROR_POST_DATA';
