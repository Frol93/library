import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// combines multiple reducers into one reducer
const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

export default store;
