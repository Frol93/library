import * as ActionTypes from '../actions/ActionTypes';

const initialState = {
	errorFetchingData: false,
	errorPostingData: false
};

const errorReducer = (state = initialState, action) => {
	switch (action.type) {
		case ActionTypes.ERROR_FETCH_DATA:
			return {
				...state,
				errorFetchingData: action.error
			};
		case ActionTypes.ERROR_POST_DATA:
			return {
				...state,
				errorPostingData: action.error
			};
		default:
			return state;
	}
};

export default errorReducer;
