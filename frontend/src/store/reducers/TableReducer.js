import * as ActionTypes from '../actions/ActionTypes';

const initialState = {
	tableData: [],
	page: 0,
	totalPages: 0,
	dataIsFetching: false
};

const tableReducer = (state = initialState, action) => {
	switch (action.type) {
		case ActionTypes.SET_TABLE_DATA:
			return {
				...state,
				tableData: action.data,
				totalPages: action.totalPages
			};
		case ActionTypes.ON_CHANGE_PAGE:
			return {
				...state,
				page: state.page + action.value
			};
		case ActionTypes.RESET_PAGE:
			return {
				...state,
				page: 0
			};

		case ActionTypes.TABLE_SPINNER:
			return {
				...state,
				dataIsFetching: action.value
			};
		default:
			return state;
	}
};

export default tableReducer;
