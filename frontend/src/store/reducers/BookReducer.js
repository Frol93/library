import * as ActionTypes from '../actions/ActionTypes';

const initialState = {
	book: {
		name: {
			element: 'input',
			type: 'text',
			value: ''
		},
		edition: {
			element: 'input',
			type: 'number',
			value: ''
		},
		writer: {
			element: 'input',
			type: 'text',
			value: ''
		},
		isbn: {
			element: 'input',
			type: 'text',
			value: ''
		},
		publisherId: {
			element: 'select',
			value: ''
		}
	},
	bookIsSending: false,
	publishers: []
};

const bookReducer = (state = initialState, action) => {
	switch (action.type) {
		case ActionTypes.ON_INPUT_CHANGE:
			return {
				...state,
				book: {
					...state.book,
					[action.elementName]: { ...state.book[action.elementName], value: action.event }
				}
			};
		case ActionTypes.RESET_BOOK_VALUES:
			return {
				...state,
				book: initialState.book
			};
		case ActionTypes.SET_PUBLISHERS:
			return {
				...state,
				publishers: action.publishers
			};
		case ActionTypes.BOOK_SPINNER:
			return {
				...state,
				bookIsSending: action.value
			};
		default:
			return state;
	}
};

export default bookReducer;
