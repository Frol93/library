import * as ActionTypes from '../actions/ActionTypes';

const initialState = {
	search: {
		name: {
			element: 'input',
			type: 'text',
			value: ''
		},
		writer: {
			element: 'input',
			type: 'text',
			value: ''
		},
		votes: {
			element: 'input',
			type: 'number',
			value: ''
		}
	}
};

const searchReducer = (state = initialState, action) => {
	switch (action.type) {
		case ActionTypes.ON_SEARCH_INPUT_CHANGE:
			return {
				...state,
				search: {
					...state.search,
					[action.elementName]: { ...state.search[action.elementName], value: action.event }
				}
			};
		default:
			return state;
	}
};

export default searchReducer;
