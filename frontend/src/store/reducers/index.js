import { combineReducers } from 'redux';
import bookReducer from './BookReducer';
import searchReducer from './SearchReducer';
import tableReducer from './TableReducer';
import errorReducer from './ErrorReducer';

export default combineReducers({ bookReducer, searchReducer, tableReducer, errorReducer });
