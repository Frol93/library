import React from 'react';
import './App.css';
import NewBook from './containers/NewBook/NewBook';
import SearchCriteria from './components/SearchCriteria/SearchCriteria';
import Table from './containers/Table/Table';
import EditBook from './containers/EditBook/EditBook';
import { Route } from 'react-router-dom';

function App() {
	return (
		<React.Fragment>
			<div className="container">
				{/* renders components based on current path */}
				<Route
					exact
					path="/"
					render={(props) => (
						<React.Fragment>
							<div className="row">
								<NewBook {...props} />
								<SearchCriteria {...props} />
							</div>
							<Table />
						</React.Fragment>
					)}
				/>
				<Route path="/edit/:id" component={EditBook} />
			</div>
		</React.Fragment>
	);
}

export default App;
