import React from 'react';
// component used to render an appropriate input, based on passed props
const input = (props) => {
	let element;

	switch (props.element) {
		case 'input':
			element = (
				<input
					className="form-control"
					id={props.elementName}
					type={props.type}
					value={props.value}
					onChange={(event) => props.change(event, props.elementName)}
				/>
			);
			break;
		case 'select':
			const options = props.options.map((option) => {
				return (
					<option key={option.id} value={option.id}>
						{option.name}
					</option>
				);
			});
			element = (
				<select
					className="form-control"
					id={props.elementName}
					value={props.value}
					onChange={(event) => props.change(event, props.elementName)}
					options={options}
				>
					<option />
					{options}
				</select>
			);
			break;
		default:
			return <div>Non existent element type</div>;
	}

	let elementName = props.elementName.charAt(0).toUpperCase() + props.elementName.slice(1);

	if (elementName === 'Isbn') elementName = elementName.toUpperCase();
	if (elementName === 'PublisherId') elementName = 'Publisher';

	return (
		<div className="form-group">
			<label htmlFor={props.elementName}>{elementName}</label>
			{element}
		</div>
	);
};

export default input;
