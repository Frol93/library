import React from 'react';
import styles from './Backdrop.module.css';
import Button from '../Button/Button';
// component used for dimming the screen and displaying an appropriate error message
const backdrop = (props) => (
	<div className={styles.Backdrop}>
		<div className={styles.ErrorMessage}>
			<p>{props.text}</p>
			<Button text="OK" type="outline-danger" click={props.click} />
		</div>
	</div>
);

export default backdrop;
