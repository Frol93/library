import React from 'react';

const button = (props) => (
	<button className={`btn btn-${props.type}`} onClick={props.click} disabled={props.disabled}>
		{props.text}
	</button>
);

export default button;
