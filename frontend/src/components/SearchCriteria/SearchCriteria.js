import React from 'react';
import Input from '../Input/Input';
import Button from '../UI/Button/Button';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/actions';
// component used for rendering search criteria inputs
const searchCriteria = (props) => {
	// disables search button if all inputs are empty
	const willDisable = Object.keys(props.search).every((key) => props.search[key].value.trim() === '');

	// generates inputs from props
	const searchCriteria = Object.keys(props.search).map((key) => {
		return (
			<Input
				key={key}
				elementName={key}
				value={props.search[key].value}
				change={props.onChange}
				element={props.search[key].element}
				type={props.search[key].type}
			/>
		);
	});

	return (
		<div className="col-lg-6">
			{searchCriteria}
			<Button
				type="primary"
				click={() => props.resetPage() && props.getData()}
				text="Search"
				disabled={willDisable}
			/>
		</div>
	);
};

const mapStateToProps = (state) => {
	return {
		search: state.searchReducer.search
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		// handles input change event
		onChange: (event, elementName) => dispatch(actions.onSearchInputChange(event, elementName)),
		// get data from db with passed search queries
		getData: () => dispatch(actions.onGetTableData()),
		// resets the table page field to 0
		resetPage: () => dispatch(actions.resetPage())
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(searchCriteria);
