package com.frol.library.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.frol.library.model.Publisher;
import com.frol.library.service.PublisherService;
import com.frol.library.support.PublisherToPublisherDTO;
import com.frol.library.web.dto.PublisherDTO;

@RestController
@RequestMapping("/api/publishers")
public class ApiPublisherController {

	@Autowired
	private PublisherService publisherService;
	@Autowired
	private PublisherToPublisherDTO toDto;
	
	@GetMapping()
	public ResponseEntity<List<PublisherDTO>> findAll(){
		
		List<Publisher> publishers = publisherService.findAll();
		
		return new ResponseEntity<>(toDto.convert(publishers), HttpStatus.OK);
	}

	@GetMapping(value="/{id}")
	public ResponseEntity<PublisherDTO> getOne(@PathVariable Long id){
		
		Publisher publisher = publisherService.findOne(id);
		
		return new ResponseEntity<>(toDto.convert(publisher), HttpStatus.OK);
	}
}
