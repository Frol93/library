package com.frol.library.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.frol.library.model.Book;
import com.frol.library.service.BookService;
import com.frol.library.support.BookDTOToBook;
import com.frol.library.support.BookToBookDTO;
import com.frol.library.web.dto.BookDTO;

@RestController
@RequestMapping("/api/books")
public class ApiBookController {

	@Autowired
	private BookService bookService;
	@Autowired
	private BookToBookDTO toDto;
	@Autowired
	private BookDTOToBook toBook;

	@GetMapping
	public ResponseEntity<List<BookDTO>> findAll(@RequestParam(required = false) String name,
			@RequestParam(required = false) String writer, @RequestParam(required = false) Integer votes,
			@RequestParam(defaultValue = "0") int page) {

		Page<Book> books;
		
		if(name != null || writer != null || votes != null) {
			books = bookService.search(name, writer, votes, page);
		} else {
			books = bookService.findAll(page);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.set("totalPages", Integer.toString(books.getTotalPages()));
		
		return new ResponseEntity<>(toDto.convert(books.getContent()), headers, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<BookDTO> findOne(@PathVariable Long id) {

		Book book = bookService.findOne(id);

		return new ResponseEntity<>(toDto.convert(book), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<BookDTO> save(@RequestBody BookDTO dto) {

		Book book = bookService.save(toBook.convert(dto));

		return new ResponseEntity<>(toDto.convert(book), HttpStatus.CREATED);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity<BookDTO> edit(@RequestBody BookDTO dto, @PathVariable Long id) {

		if (!dto.getId().equals(id)) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Book book = bookService.save(toBook.convert(dto));

		return new ResponseEntity<>(toDto.convert(book), HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {

		bookService.delete(id);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
