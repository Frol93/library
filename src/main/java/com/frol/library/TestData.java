package com.frol.library;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frol.library.model.Book;
import com.frol.library.model.Publisher;
import com.frol.library.service.BookService;
import com.frol.library.service.PublisherService;

@Component
public class TestData {

	@Autowired
	private PublisherService publisherService; 
	@Autowired
	private BookService bookService;
	
	@PostConstruct
	public void init() {
		for(int i = 0; i < 7; i++) {
			Publisher publisher = new Publisher();
			publisher.setAddress("Address " + i);
			publisher.setName("Name " + i);
			publisher.setPhoneNum("Phone Number " + i);
			
			for(int j = 0; j < 10; j++) {
				Book book = new Book();
				book.setEdition(j * i);
				book.setIsbn("ISBN " + i + j);
				book.setName("Name " + j);
				book.setPublisher(publisher);
				book.setWriter("Writer " + j);
				publisherService.save(publisher);
				bookService.save(book);
			}
		}
	}
}
