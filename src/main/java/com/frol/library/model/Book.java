package com.frol.library.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Book {

	@Id
	@GeneratedValue
	private Long id;
	@Column(nullable=false)
	private String name;
	@Column(nullable=false)
	private int edition;
	@Column(nullable=false)
	private String writer;
	@Column(nullable=false, unique=true)
	private String isbn;
	private int votes;
	@ManyToOne
	private Publisher publisher;
	
	public Book() {}

	public Book(Long id, String name, int edition, String writer, String isbn, int votes, Publisher publisher) {
		super();
		this.id = id;
		this.name = name;
		this.edition = edition;
		this.writer = writer;
		this.isbn = isbn;
		this.votes = votes;
		this.publisher = publisher;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEdition() {
		return edition;
	}

	public void setEdition(int edition) {
		this.edition = edition;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getVotes() {
		return votes;
	}

	public void setVotes(int votes) {
		this.votes = votes;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
		if(!publisher.getBooks().contains(this)) {
			publisher.getBooks().add(this);
		}
	}
}
