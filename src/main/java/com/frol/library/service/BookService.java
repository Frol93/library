package com.frol.library.service;

import org.springframework.data.domain.Page;

import com.frol.library.model.Book;

public interface BookService {

	Page<Book> findAll(int page);
	Book findOne(Long id);
	Book save(Book book);
	void delete(Long id);
	Page<Book> search(String name, String writer, Integer votes, int page);
}
