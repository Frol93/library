package com.frol.library.service;

import java.util.List;

import com.frol.library.model.Publisher;

public interface PublisherService {

	List<Publisher> findAll();
	Publisher findOne(Long id);
	void save(Publisher publisher);
}
