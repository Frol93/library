package com.frol.library.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.frol.library.model.Book;
import com.frol.library.repository.BookRepository;
import com.frol.library.service.BookService;

@Service
public class JpaBookService implements BookService{

	@Autowired
	private BookRepository bookRepository; 
	
	@Override
	public Page<Book> findAll(int page) {
		
		return bookRepository.findAll(PageRequest.of(page, 5));
	}
	@Override
	public Book findOne(Long id) {
		
		return bookRepository.getOne(id);
	}

	@Override
	public Book save(Book book) {
		
		Book savedBook = bookRepository.save(book);
		
		return savedBook;
	}

	@Override
	public void delete(Long id) {
		
		bookRepository.deleteById(id);
		
	}

	

	@Override
	public Page<Book> search(String name, String writer, Integer votes, int page) {
		
		if(name != null) {
			name = "%" + name + "%";
		}
		
		if(writer != null) {
			writer = "%" + writer + "%";
		}
		
		return bookRepository.search(name, writer, votes, PageRequest.of(page, 5));
	}
	
	

	
}
