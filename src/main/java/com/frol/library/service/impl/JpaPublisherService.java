package com.frol.library.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.frol.library.model.Publisher;
import com.frol.library.repository.PublisherRepository;
import com.frol.library.service.PublisherService;

@Service
public class JpaPublisherService implements PublisherService {

	@Autowired
	private PublisherRepository publisherRepository;

	@Override
	public List<Publisher> findAll() {

		return publisherRepository.findAll();
	}

	@Override
	public Publisher findOne(Long id) {

		return publisherRepository.getOne(id);
	}

	@Override
	public void save(Publisher publisher) {
	
		publisherRepository.save(publisher);
	}

}
