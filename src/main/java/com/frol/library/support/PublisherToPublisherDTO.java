package com.frol.library.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.frol.library.model.Publisher;
import com.frol.library.web.dto.PublisherDTO;

@Component
public class PublisherToPublisherDTO implements Converter<Publisher, PublisherDTO> {

	@Override
	public PublisherDTO convert(Publisher publisher) {
		PublisherDTO dto = new PublisherDTO();

		dto.setAddress(publisher.getAddress());
		dto.setId(publisher.getId());
		dto.setName(publisher.getName());
		dto.setPhoneNum(publisher.getPhoneNum());

		return dto;
	}

	public List<PublisherDTO> convert(List<Publisher> publishers) {
		List<PublisherDTO> dto = new ArrayList<>();

		for (Publisher p : publishers) {
			dto.add(convert(p));
		}

		return dto;
	}
}
