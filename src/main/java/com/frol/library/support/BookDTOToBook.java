package com.frol.library.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.frol.library.model.Book;
import com.frol.library.model.Publisher;
import com.frol.library.service.BookService;
import com.frol.library.service.PublisherService;
import com.frol.library.web.dto.BookDTO;

@Component
public class BookDTOToBook implements Converter<BookDTO, Book> {

	@Autowired
	private PublisherService publisherService;
	@Autowired
	private BookService bookService;

	@Override
	public Book convert(BookDTO dto) {

		Publisher publisher = publisherService.findOne(dto.getPublisherId());

		if (publisher != null) {
			Book book;
			
			if(dto.getId() == null) {
				book = new Book();
			} else {
				book = bookService.findOne(dto.getId());
			}
			
			book.setEdition(dto.getEdition());
			book.setId(dto.getId());
			book.setIsbn(dto.getIsbn());
			book.setName(dto.getName());
			book.setPublisher(publisher);
			book.setVotes(dto.getVotes());
			book.setWriter(dto.getWriter());
			
			return book;
		} else {
			throw new NullPointerException("Trying to attach a non existent entity");
		}

	}

}
