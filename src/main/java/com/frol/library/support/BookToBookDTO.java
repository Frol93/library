package com.frol.library.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.frol.library.model.Book;
import com.frol.library.web.dto.BookDTO;

@Component
public class BookToBookDTO implements Converter<Book, BookDTO>{

	@Override
	public BookDTO convert(Book book) {
		BookDTO dto = new BookDTO();
		
		dto.setEdition(book.getEdition());
		dto.setId(book.getId());
		dto.setIsbn(book.getIsbn());
		dto.setName(book.getName());
		dto.setPublisherId(book.getPublisher().getId());
		dto.setPublisherName(book.getPublisher().getName());
		dto.setVotes(book.getVotes());
		dto.setWriter(book.getWriter());
		
		return dto;
	}

	public List<BookDTO> convert(List<Book> books){
		List<BookDTO> dto = new ArrayList<>();
		
		for(Book b : books) {
			dto.add(convert(b));
		}
		
		return dto;
	}
}
