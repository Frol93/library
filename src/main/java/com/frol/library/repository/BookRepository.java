package com.frol.library.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.frol.library.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>{
	
	@Query("SELECT b FROM Book b WHERE "
			+ "(:name IS NULL OR b.name LIKE :name) AND "
			+ "(:writer IS NULL OR b.writer LIKE :writer) AND "
			+ "(:votes IS NULL OR b.votes >= :votes)")
	Page<Book> search(@Param("name") String name, @Param("writer") String writer, @Param("votes") Integer votes, Pageable pageable);

}
